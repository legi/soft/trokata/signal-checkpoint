# Signal_Checkpoint - Module Fortran 90 de gestion des signaux à usage du calcul

**```Signal_Checkpoint```** est un module en Fortran 90 permettant une gestion basique des signaux POSIX (UNIX).
L'utilisation est très orienté calcul.
L'idée est que le scheduleur de job envoi avant la fin du walltime un signal au job (checkpoint),
le job finit alors proprement.
S'il a été lancé avec l'option idempotent (par exemple sous [http://oar.imag.fr/ OAR] : "```oarsub -t idempotent```"),
le scheduleur le relance alors automatiquement...

Quelques sous pages permettent d'en savoir plus sur ```Signal_Checkpoint```.

 * [wiki:SoftWare/SignalCheckpoint/API] - Application Programming Interface
 * [wiki:SoftWare/SignalCheckpoint/OAR] - Utilisation avec le scheduleur OAR

## Module

Le module **[source:/trunk/signal-checkpoint/signal_checkpoint.F90 Signal_Checkpoint]**
gère pour le moment les signaux ```SIGHUP``` ```SIGINT``` ```SIGQUIT``` ```SIGUSR1``` ```SIGUSR2```, ```SIGTERM``` et ```SIGXCPU```
(voir : "```kill -l```", pour obtenir la liste des signaux gérée par votre système).
A noter qu'[http://oar.imag.fr/ OAR] utilise par défaut le signal ```SIGUSR2``` lors du checkpointing.
Il a été testé et validé avec les compilateurs ```GNU/gfortran```, ```Intel/ifort``` et ```IBM/xlf```.
Il fait un appel POSIX à la commande ```signal``` qui n'est pas normalisé en Fortran,
en ce sens, ce n'est pas du pur Fortran...
Afin d'éviter tout risque, la bibliothèque ne fait absolument rien sous les compilateurs non validés !

Cependant, coté utilisateur, l'API est très simple et indépendante du compilateur.

Le programme peux s'abonner à plusieurs signaux
bien qu'il n'y ai qu'un seul compteur (celui-ci est donc multiplexé).
Cela permet d'avoir un programme s'arrêtant proprement via deux signaux : ```SIGUSR1``` et ```SIGUSR2``` par exemple.
Le premier signal (```SIGUSR1```) arrête le programme via une volonté de l'utilisateur,
alors que le second (```SIGUSR2```) est lancé via le scheduleur...

Le projet est très jeune.
Il n'y a pas de bibliothèque de compilé pour le moment.
Pour l'intégrer dans votre projet, le plus simple pour le moment est de l'intégrer dans vos sources.
A noter qu'il s'agit d'un fichier ayant l'extension ```.F90``` et non ```.f90```, il doit donc être pré-compilé...
Normalement, les compilateurs modernes le font automatiquement !
```bash
wget http://servforge.legi.grenoble-inp.fr/svn/soft-trokata/trunk/signal-checkpoint/signal_checkpoint.F90
```

## Test

Un programme de test et d'exemple [source:/trunk/signal-checkpoint/test_sgck1.f90 test_sgck1] est fournit.
Pour le récupérer et le tester
```bash
mkdir test_sgck1
cd test_sgck1
wget http://servforge.legi.grenoble-inp.fr/svn/soft-trokata/trunk/signal-checkpoint/test_sgck1.f90
wget http://servforge.legi.grenoble-inp.fr/svn/soft-trokata/trunk/signal-checkpoint/signal_checkpoint.F90
```

 * compilation avec ```GNU/gfortran```
 ```
rm test_sgck1 signal_checkpoint.mod signal_checkpoint.o
gfortran -c signal_checkpoint.F90
gfortran -o test_sgck1 test_sgck1.f90 signal_checkpoint.o
```

 * compilation avec ```Intel/ifort```
 ```
rm test_sgck1 signal_checkpoint.mod signal_checkpoint.o
ifort -c signal_checkpoint.F90
ifort -o test_sgck1 test_sgck1.f90 signal_checkpoint.o
```

 * compilation avec ```IBM/xlf```
 ```
rm test_sgck1 signal_checkpoint.mod signal_checkpoint.o
xlf95 -c signal_checkpoint.F90
xlf95 -o test_sgck1 test_sgck1.f90 signal_checkpoint.o
```

Pour tester le programme, il faut deux terminaux.
Le programme de test réagit aux signaux ```SIGUSR1``` et ```SIGUSR2``` indifféremment.

 * Terminal 1 - celui où la commande tourne réellement.
 ```
./test_sgck1
```

 * Terminal 2 - celui qui joue le rôle du scheduleur.
 A noter que la commande ```ps``` n'est pas toujours celle du ```GNU``` partout
 (exemple, IBM AIX de l'IDRIS).
 ```
ps fux | grep ./test_sgck1 | grep -v grep | awk '{print $2}' | xargs kill -USR2
```

Le script **```bash```** suivant lance automatiquement le programme
et lui envoi ensuite le signal ```USR2``` puis ```USR1``` pour l'arrêter.
Il est programmé à la manière d'un job pour un scheduleur,
c'est à dire il s'envoie les signaux à lui même et utilise des trap en interne.
A noter que le script est aussi valide sous **```ksh```**,
il suffit de remplacer dans le shebang le chemin de ```bash``` par celui de ```ksh```.
En effet, les fonctions intégrées au shell nécessaires
(```trap``` pour détourner les signaux et ```jobs``` pour avoir la liste des sous processus)
sont identiques dans les deux shells.
```bash
wget http://servforge.legi.grenoble-inp.fr/svn/soft-trokata/trunk/signal-checkpoint/test_sgck1.sh
chmod u+rx test_sgck1.sh
./test_sgck1.sh
```
Il est inutile d'utiliser les shells basé sur **```csh```** (```tcsh```).
A notre connaissance, ces shells ne savent pas gérer les signaux sauf de manière très basique via ```onintr```
ce qui est totalement insuffisant ici.
A vrai dire, le ```csh``` est complètement dépassé...

## Repository

L'ensemble du code est sous **licence libre**.
les modules sources ```Fortran90``` sont sous LGPL version 2 ou plus récente
(sans restriction d'édition de lien avec la GPL v3 par exemple),
les programmes de tests en ```Fortran90``` sont dans le domaine public.

Tous les sources sont disponibles sur la forge du LEGI :
http://servforge.legi.grenoble-inp.fr/svn/soft-trokata/trunk/signal-checkpoint

Les sources sont gérés via subversion (http://subversion.tigris.org/).
Il est très facile de rester synchronisé par rapport à ces sources.

 * la récupération initiale
```bash
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/legi/soft/trokata/signal-checkpoint
```
 * les mises à jour par la suite
```bash
git pull
```

Il est possible d'avoir un accès en écriture à la forge
sur demande motivée à [Gabriel Moreau](mailto:Gabriel.Moreau__AT__legi.grenoble-inp.fr).
Pour des questions de temps d'administration et de sécurité,
la forge n'est pas accessible en écriture sans autorisation.
