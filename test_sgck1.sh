#!/bin/bash
#
# 2012/04/23 gabriel

trap 'kill -USR2 $(jobs -p)' USR2
trap 'kill -USR1 $(jobs -p)' USR1


./test_sgck1 &


sleep 20

kill -USR2 $$
sleep 2
kill -USR1 $$
sleep 1
kill -USR2 $$

sleep 10
