! 2012/04/20 (C) Gabriel Moreau
!
! rm sgck_test1 *.o *.mod
! gfortran -c signal_checkpoint.F90 && gfortran -o sgck_test1 sgck_test1.f90 signal_checkpoint.o
! ./sgck_test1
!
! ps fux | grep ./sgck_test1 | grep -v grep | awk '{print $2}' | xargs kill -USR2
!
! ifort -c signal_checkpoint.F90 && ifort -o sgck_test1 sgck_test1.f90 signal_checkpoint.o

program sgck_test1

   use Signal_Checkpoint, only: &
      signal_checkpoint_connect, &
      signal_checkpoint_is_received, &
      signal_checkpoint_received_times, &
      signal_checkpoint_ask_for_exit_code, &
      SIGUSR2, &
      SIGUSR1

   integer :: I

   call signal_checkpoint_connect(SIGUSR2)
   call signal_checkpoint_connect(SIGUSR1, EXIT=.true.)

   print *, "initialising phase"

   do while (.not. signal_checkpoint_is_received())
      do I = 1, 15
         print *, "runnning in the loop ", I, signal_checkpoint_received_times()
         call sleep (1)
      end do
   end do

   print *, "finishing phase"

   if (signal_checkpoint_ask_for_exit_code()) then
     print *, "stop with exit code 1"
     stop 1
   end if
end program
